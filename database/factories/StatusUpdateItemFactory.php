<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use TshirtAndSons\StatusUpdates\Models\StatusUpdateItem;

class StatusUpdateItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StatusUpdateItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'status_update_id' => 1,
            'item_uuid'        => Str::uuid(),
            'item_ref'         => 'test-item-ref',
            'invoice_price'    => null
        ];
    }
}
