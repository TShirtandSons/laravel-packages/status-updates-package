<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use TshirtAndSons\StatusUpdates\Models\StatusUpdate;

class StatusUpdateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StatusUpdate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'order_uuid'      => Str::uuid(),
            'order_ref'       => 'test-order-ref',
            'status'          => 'picked',
            'part_shipped'    => false,
            'shipping_method' => 'Standard',
            'carrier'         => null,
            'tracking_number' => null,
            'tracking_url'    => null,
            'date_shipped'    => null,
            'shipping_cost'   => null,
            'handled_at'      => null
        ];
    }
}
