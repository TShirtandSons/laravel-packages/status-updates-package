<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddItemUuidToStatusUpdateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('status_update_items', function (Blueprint $table) {
            $table->uuid('item_uuid')->after('status_update_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('status_update_items', function (Blueprint $table) {
            $table->removeColumn('item_uuid');
        });
    }
}
