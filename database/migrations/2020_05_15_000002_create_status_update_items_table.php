<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusUpdateItemsTable extends Migration
{
    public function up()
    {
        Schema::create('status_update_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('status_update_id')->unsigned();
            $table->string('item_ref',50);
            $table->decimal('invoice_price', 5, 2)->nullable();
            $table->timestamps();

            $table->foreign('status_update_id')
                ->references('id')
                ->on('status_updates')
                ->onDelete('cascade');
        });
    }


    public function down()
    {
        Schema::dropIfExists('status_update_items');
    }
}
