<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusUpdatesTable extends Migration
{
    public function up()
    {
        Schema::create('status_updates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->nullable();
            $table->string('order_ref',50);
            $table->boolean('part_shipped');
            $table->string('status', 25);
            $table->string('shipping_method', 25);
            $table->string('carrier')->nullable();
            $table->string('tracking_number', 50)->nullable();
            $table->text('tracking_url')->nullable();
            $table->date('date_shipped')->nullable();
            $table->decimal('shipping_cost', 5, 2)->nullable();
            $table->timestamp('handled_at')->nullable()->default(null);
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('status_updates');
    }
}
