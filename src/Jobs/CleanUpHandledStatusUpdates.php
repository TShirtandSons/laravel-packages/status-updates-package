<?php

namespace TshirtAndSons\StatusUpdates\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use TshirtAndSons\StatusUpdates\Models\StatusUpdate;

class CleanUpHandledStatusUpdates implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Carbon $processFromDate;

    /**
     * CleanUpHandledStatusUpdates constructor.
     *
     * @param Carbon $processFromDate
     */
    public function __construct(Carbon $processFromDate)
    {
        $this->processFromDate = $processFromDate;
    }

    /**
     * Handle the job
     */
    public function handle()
    {
        StatusUpdate::cleanupHandledEntries($this->processFromDate);
    }
}
