<?php

namespace TshirtAndSons\StatusUpdates\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;

class BearerTokenAuth
{
    /**
     * Handle the token in the auth bearer header
     *
     * @param $request
     * @param Closure $next
     *
     * @return JsonResponse|mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->bearerToken();

        if ($token == config('tsas.token')) {
            return $next($request);
        }

        return response()->json(['result' => 'unauthorised'], 401);
    }
}
