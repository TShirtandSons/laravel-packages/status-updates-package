<?php

namespace TshirtAndSons\StatusUpdates\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TshirtAndSons\StatusUpdates\Events\StatusUpdateWasReceived;
use TshirtAndSons\StatusUpdates\Http\Middleware\BearerTokenAuth;
use TshirtAndSons\StatusUpdates\Models\StatusUpdate;
use TshirtAndSons\StatusUpdates\Models\StatusUpdateItem;

class StatusUpdateController extends Controller
{
    /**
     * StatusUpdateController constructor.
     */
    public function __construct()
    {
        $this->middleware(BearerTokenAuth::class);
    }

    /**
     * Create the status update with the incoming request data
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        /*
         * If we have a StatusUpdate matching the order_ref and status provided and we have already handled it, just return.
         * T Shirt & Sons are expecting to receive a 200 range response if the update is received.
         */
        $order_ref = $request->input('order.order_ref');
        $version = 2;
        if ($request->input('order.id')) {
            $order_ref = $request->input('order.id');
            $version = 1;
        }
        if (
            StatusUpdate::statusUpdateExistsForThisOrderAndStatus(
                $order_ref,
                $request->input('order.status')
            )
        ) {
            return response()->json('Update already processed');
        }

        DB::beginTransaction();
        try {
            $statusUpdate = new StatusUpdate();
            if ($version === 1) {
                // v1
                $statusUpdate->order_ref = $request->input('order.id');
                $statusUpdate->part_shipped = 'unknown';
                $statusUpdate->shipping_method = 'unknown';
                $statusUpdate->carrier = $request->input('order.carrier');
                $statusUpdate->tracking_number = $request->input('order.tracking.0.number');
            } else {
                // v2
                $statusUpdate->order_ref = $request->input('order.order_ref');
                $statusUpdate->shipping_method = $request->input('order.shipping_method');
                $statusUpdate->carrier = $request->input('order.carrier');
                $statusUpdate->tracking_number = $request->input('order.tracking_number');
                $statusUpdate->part_shipped = $request->input('order.part_shipment');
                $statusUpdate->tracking_url = $request->input('order.tracking_url');
                $statusUpdate->date_shipped = $request->input('order.shipped_date');
                $statusUpdate->shipping_cost = $request->input('order.shipping_cost');
            }
            // Standard across all formats
            $statusUpdate->order_uuid = $request->input('order.uuid');
            $statusUpdate->status = $request->input('order.status');
            $statusUpdate->save();

            if ($request->input('order.item')) {
                foreach ($request->input('order.item') as $item) {
                    // v1
                    $statusUpdateItem = new StatusUpdateItem();
                    $statusUpdateItem->status_update_id = $statusUpdate->id;
                    $statusUpdateItem->item_ref = $item['id'];
                    if (isset($item['uuid'])) {
                        $statusUpdateItem->item_uuid = $item['uuid'];
                    }
                    $statusUpdateItem->save();
                }
            } else {
                foreach ($request->input('order.items') as $item) {
                    // v2
                    $statusUpdateItem = new StatusUpdateItem();
                    $statusUpdateItem->status_update_id = $statusUpdate->id;
                    $statusUpdateItem->item_uuid = $item['uuid'] ?? null;
                    $statusUpdateItem->item_ref = $item['item_ref'];
                    $statusUpdateItem->invoice_price = $item['invoice_price'];
                    $statusUpdateItem->save();
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json($e->getMessage(), 500);
        }

        StatusUpdateWasReceived::dispatch($statusUpdate);

        return response()->json('ok', 201);
    }
}
