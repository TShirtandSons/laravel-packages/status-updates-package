<?php

namespace TshirtAndSons\StatusUpdates\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use TshirtAndSons\StatusUpdates\Models\StatusUpdate;

class StatusUpdateWasReceived
{
    use Dispatchable, SerializesModels;

    public StatusUpdate $statusUpdate;

    /**
     * StatusUpdateWasReceived constructor.
     *
     * @param StatusUpdate $statusUpdate
     */
    public function __construct(StatusUpdate $statusUpdate)
    {
        $this->statusUpdate = $statusUpdate;
    }
}
