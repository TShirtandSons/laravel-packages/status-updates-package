<?php

namespace TshirtAndSons\StatusUpdates;

use Illuminate\Support\ServiceProvider;

class StatusUpdateServiceProvider extends ServiceProvider
{
    /**
     * @inheritDoc
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'tsas'
        );
    }

    /**
     * Boot up
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            // publish our migrations
            $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        }

        // publish our config
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('tsas.php'),
        ], 'config');

        // Publish our routes
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
    }
}
