<?php

namespace TshirtAndSons\StatusUpdates\Models;

use Database\Factories\StatusUpdateItemFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Class StatusUpdateItem
 * @package TshirtAndSons\StatusUpdates\Models
 *
 * @property int $id
 * @property int $status_update_id
 * @property string|null $item_uuid
 * @property string $item_ref
 * @property float|null $invoice_price
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read StatusUpdate $statusUpdate
 * @method static Builder|StatusUpdateItem newModelQuery()
 * @method static Builder|StatusUpdateItem newQuery()
 * @method static Builder|StatusUpdateItem query()
 * @method static Builder|StatusUpdateItem whereStatusUpdateId($value)
 * @method static Builder|StatusUpdateItem whereItemUuid($value)
 * @method static Builder|StatusUpdateItem whereItemRef($value)
 * @method static Builder|StatusUpdateItem whereInvoicePrice($value)
 * @method static Builder|StatusUpdateItem whereCreatedAt($value)
 * @method static Builder|StatusUpdateItem whereUpdatedAt($value)
 */
class StatusUpdateItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'status_update_id',
        'item_uuid',
        'item_ref',
        'invoice_price',
    ];

    protected $casts = [
        'id'               => 'integer',
        'status_update_id' => 'integer',
        'item_uuid'        => 'string',
        'item_ref'         => 'string',
        'invoice_price'    => 'string',
    ];

    /**
     * @inheritDoc
     *
     * @return StatusUpdateItemFactory
     */
    protected static function newFactory(): StatusUpdateItemFactory
    {
        return StatusUpdateItemFactory::new();
    }

    /**
     * Get the update this item belongs to
     *
     * @return BelongsTo
     */
    public function statusUpdate(): BelongsTo
    {
        return $this->belongsTo(StatusUpdate::class);
    }
}
