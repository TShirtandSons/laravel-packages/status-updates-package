<?php

namespace TshirtAndSons\StatusUpdates\Models;

use Database\Factories\StatusUpdateFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Class StatusUpdate
 * @package TshirtAndSons\StatusUpdates\Models
 *
 * @property int $id
 * @property int|null $order_id
 * @property string|null $order_uuid
 * @property string $order_ref
 * @property bool $part_shipped
 * @property string $status
 * @property string $shipping_method
 * @property string|null $carrier
 * @property string|null $tracking_number
 * @property string|null $tracking_url
 * @property Carbon|null $date_shipped
 * @property float|null $shipping_cost
 * @property Carbon|string|null $handled_at
 * @property Carbon|string|null $created_at
 * @property Carbon|string|null $updated_at
 * @property-read Collection|StatusUpdateItem[] $statusUpdateItems
 * @method static Builder|StatusUpdate newModelQuery()
 * @method static Builder|StatusUpdate newQuery()
 * @method static Builder|StatusUpdate query()
 * @method static Builder|StatusUpdate latestForOrderRefWithItems($orderRef)
 * @method static Builder|StatusUpdate whereOrderId($value)
 * @method static Builder|StatusUpdate whereOrderUuid($value)
 * @method static Builder|StatusUpdate whereOrderRef($value)
 * @method static Builder|StatusUpdate wherePartShipped($value)
 * @method static Builder|StatusUpdate whereStatus($value)
 * @method static Builder|StatusUpdate whereShippingMethod($value)
 * @method static Builder|StatusUpdate whereCarrier($value)
 * @method static Builder|StatusUpdate whereTrackingNumber($value)
 * @method static Builder|StatusUpdate whereTrackingUrl($value)
 * @method static Builder|StatusUpdate whereDateShipped($value)
 * @method static Builder|StatusUpdate whereShippingCost($value)
 * @method static Builder|StatusUpdate whereHandledAt($value)
 * @method static Builder|StatusUpdate whereCreatedAt($value)
 * @method static Builder|StatusUpdate whereUpdatedAt($value)
 */
class StatusUpdate extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'order_uuid',
        'order_ref',
        'part_shipped',
        'status',
        'shipping_method',
        'carrier',
        'tracking_number',
        'tracking_url',
        'date_shipped',
        'shipping_cost',
        'handled_at',
    ];

    protected $casts = [
        'id'              => 'integer',
        'order_id'        => 'integer',
        'order_uuid'      => 'string',
        'order_ref'       => 'string',
        'part_shipped'    => 'boolean',
        'status'          => 'string',
        'shipping_method' => 'string',
        'carrier'         => 'string',
        'tracking_number' => 'string',
        'tracking_url'    => 'string',
        'date_shipped'    => 'date',
        'shipping_cost'   => 'decimal:2',
        'handled_at'      => 'datetime',
    ];

    /**
     * @inheritDoc
     *
     * @return StatusUpdateFactory
     */
    protected static function newFactory(): StatusUpdateFactory
    {
        return StatusUpdateFactory::new();
    }

    /**
     * Get all the items for this status update
     *
     * @return HasMany
     */
    public function statusUpdateItems(): HasMany
    {
        return $this->hasMany(StatusUpdateItem::class);
    }

    /**
     * Scope a builder for getting updates for a given ref ordered by the latest
     *
     * @param Builder $query
     * @param string $orderRef
     *
     * @return Builder
     */
    public function scopeLatestForOrderRefWithItems(Builder $query, string $orderRef): Builder
    {
        return $query
            ->where('order_ref', $orderRef)
            ->with('statusUpdateItems')
            ->latest();
    }

    /**
     * Return a collection of all unhandled updates
     *
     * @return Collection
     */
    public static function listUnhandled(): Collection
    {
        return self::query()
            ->whereNull('handled_at')
            ->orderBy('created_at')
            ->get();
    }

    /**
     * Get the latest update for the given order ref
     *
     * @param string $orderRef
     *
     * @return StatusUpdate|null
     */
    public static function getLatestForOrderRef(string $orderRef): ?StatusUpdate
    {
        return self::latestForOrderRefWithItems($orderRef)->first();
    }

    /**
     * Return a collection of all updates for the given order uuid ordered by latest
     *
     * @param string $orderRef
     *
     * @return Collection
     */
    public static function listForOrderRef(string $orderRef): Collection
    {
        return self::latestForOrderRefWithItems($orderRef)->get();
    }

    /**
     * Delete all entries that have a handled date that is before or on the given date
     *
     * @param Carbon $processFromDate
     *
     * @return int
     */
    public static function cleanupHandledEntries(Carbon $processFromDate): int
    {
        $statusUpdates = self::query()
            ->where('handled_at', '<=', $processFromDate->toDateTimeString())
            ->get();

        $statusUpdates->each(function (StatusUpdate $statusUpdate) {
            $statusUpdate->delete();
        });

        return $statusUpdates->count();
    }

    /**
     * Remove the tracking number from all entries that have a handled date that is before or on the given date and
     * have a status of shipped
     *
     * @param Carbon $processFromDate
     *
     * @return int
     */
    public static function removeTrackingNumbersFromHandledShippedEntries(Carbon $processFromDate): int
    {
        $statusUpdates = self::query()
            ->where('status', 'shipped')
            ->where('handled_at', '<=', $processFromDate->toDateTimeString())
            ->get();

        $statusUpdates->each(function (StatusUpdate $statusUpdate) {
            $statusUpdate->tracking_number = null;
            $statusUpdate->save();
        });

        return $statusUpdates->count();
    }

    /**
     * Try to get a status update entry for the given order uuid and status
     *
     * @param string $orderRef
     * @param string $status
     *
     * @return StatusUpdate|null
     */
    public static function statusUpdateExistsForThisOrderAndStatus(string $orderRef, string $status): ?StatusUpdate
    {
        return self::query()->where('order_ref', $orderRef)
            ->where('status', $status)
            ->whereNotNull('handled_at')
            ->first();
    }
}
