<?php

namespace TshirtAndSons\StatusUpdates\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use TshirtAndSons\StatusUpdates\Models\StatusUpdate;

trait HasStatusUpdates
{
    /**
     * Get all the status updates for this order
     *
     * @return HasMany
     */
    public function statusUpdates(): HasMany
    {
        return $this->hasMany(StatusUpdate::class, 'order_id', 'id');
    }
}
