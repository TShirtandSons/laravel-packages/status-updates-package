<?php

return [
    'endpoint' => env('TSAS_STATUS_UPDATE_ENDPOINT', '/receive-tss-status-update'),
    'token' => env('TSAS_BEARER_TOKEN', 'tshirtandsons')
];
