<?php

namespace TshirtAndSons\StatusUpdates\Tests\Unit;

use TshirtAndSons\StatusUpdates\Tests\StatusUpdatesTestCase;

class ConfigTest extends StatusUpdatesTestCase
{
    /** @test */
    public function the_config_file_is_loaded()
    {
        $this->assertTrue(config('tsas.endpoint') == '/receive-tss-status-update');
    }
}
