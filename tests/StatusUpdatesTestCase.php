<?php

namespace TshirtAndSons\StatusUpdates\Tests;

use Illuminate\Database\SQLiteConnection;
use Illuminate\Support\Facades\DB;
use TshirtAndSons\StatusUpdates\StatusUpdateServiceProvider;
use Orchestra\Testbench\TestCase;

abstract class StatusUpdatesTestCase extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // Allow using sqlite cascading delete in unit tests.
        if (DB::connection() instanceof SQLiteConnection) {
            DB::statement(DB::raw('PRAGMA foreign_keys=1'));
        }
    }

    protected function getPackageProviders($app)
    {
        return [
            StatusUpdateServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        // perform environment setup
        $app['config']->set('database.default', 'test_db');
        $app['config']->set(
            'database.connections.test_db',
            [
                'driver'   => 'sqlite',
                'database' => ':memory:'
            ]
        );
    }
}
