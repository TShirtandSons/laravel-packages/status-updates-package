<?php

namespace TshirtAndSons\StatusUpdates\Tests\Feature;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;
use TshirtAndSons\StatusUpdates\Events\StatusUpdateWasReceived;
use TshirtAndSons\StatusUpdates\Models\StatusUpdate;
use TshirtAndSons\StatusUpdates\Tests\StatusUpdatesTestCase;

class StatusUpdateTest extends StatusUpdatesTestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_request_without_a_token_returns_unauthorised()
    {
        $response = $this->postJson('/api'.config('tsas.endpoint'), [
            'order' => [
                'order_ref' => 'test-order-ref',
                'status' => 'picked',
                'part_shipped' => false,
                'shipping_method' => 'Standard',
                'carrier' => null,
                'tracking_number' => null,
                'tracking_url' => null,
                'shipped_date' => null,
                'shipping_cost' => null,
                'items' => [
                    0 => [
                        'item_ref' => 'test-item-ref',
                        'invoice_price' => 10.99
                    ]
                ]
            ]
        ]);

        $response->assertStatus(401);
    }


    /** @test */
    public function an_event_is_emitted_when_a_status_update_is_received()
    {
        Event::fake();

        $response = $this->postJson('/api'.config('tsas.endpoint'), [
            'order' => [
                'uuid' => Str::uuid(),
                'order_ref' => 'test-order-ref',
                'status' => 'picked',
                'part_shipment' => false,
                'shipping_method' => 'Standard',
                'carrier' => null,
                'tracking_number' => null,
                'tracking_url' => null,
                'shipped_date' => null,
                'shipping_cost' => null,
                'items' => [
                    0 => [
                        'uuid' => Str::uuid(),
                        'item_ref' => 'test-item-ref',
                        'invoice_price' => 10.99
                    ]
                ]
            ]
        ], $this->headers());

        $statusUpdate = StatusUpdate::first();

        $response->assertStatus(201);
        $this->assertDatabaseHas('status_updates', [
            'order_ref' => 'test-order-ref'
        ]);
        $this->assertDatabaseHas('status_update_items', [
            'item_ref' => 'test-item-ref',
            'invoice_price' => 10.99
        ]);

        Event::assertDispatched(StatusUpdateWasReceived::class, function ($event) use ($statusUpdate) {
            return $event->statusUpdate->id == $statusUpdate->id;
        });
    }

    /** @test */
    public function an_event_is_emitted_when_a_v1_status_update_is_received()
    {
        Event::fake();

        $response = $this->postJson('/api'.config('tsas.endpoint'), [
            'order' => [
                'uuid' => Str::uuid(),
                'id' => 'test-order-ref-v1',
                'status' => 'picked',
                'item' => [
                    0 => [
                        'uuid' => Str::uuid(),
                        'id' => 'test-item-ref-v1',
                    ]
                ]
            ]
        ], $this->headers());

        $statusUpdate = StatusUpdate::first();

        $response->assertStatus(201);
        $this->assertDatabaseHas('status_updates', [
            'order_ref' => 'test-order-ref-v1'
        ]);
        $this->assertDatabaseHas('status_update_items', [
            'item_ref' => 'test-item-ref-v1',
        ]);

        Event::assertDispatched(StatusUpdateWasReceived::class, function ($event) use ($statusUpdate) {
            return $event->statusUpdate->id == $statusUpdate->id;
        });
    }

    /** @test */
    public function an_event_is_emitted_when_a_v1_status_update_is_received_with_tracking_number()
    {
        Event::fake();

        $response = $this->postJson('/api'.config('tsas.endpoint'), [
            'order' => [
                'uuid' => Str::uuid(),
                'id' => 'test-order-ref-v1-2',
                'status' => 'picked',
                'item' => [
                    0 => [
                        'uuid' => Str::uuid(),
                        'id' => 'test-item-ref-v1-2',
                    ]
                ],
                'tracking' => [
                    0 => [
                        'number' => 'test-tracking-number'
                    ]
                ]
            ]
        ], $this->headers());

        $statusUpdate = StatusUpdate::first();

        $response->assertStatus(201);
        $this->assertDatabaseHas('status_updates', [
            'order_ref' => 'test-order-ref-v1-2',
            'tracking_number' => 'test-tracking-number'
        ]);

        Event::assertDispatched(StatusUpdateWasReceived::class, function ($event) use ($statusUpdate) {
            return $event->statusUpdate->id == $statusUpdate->id;
        });
    }


    /** @test */
    public function a_duplicated_status_update_is_not_stored()
    {
        $known_order_ref = 'test-order-ref-123';
        $known_status = 'shipped';
        $uuid = Str::uuid();

        StatusUpdate::factory()->create([
            'order_uuid' => $uuid,
            'order_ref' => $known_order_ref,
            'status' => $known_status,
            'tracking_number' => 'test_tracking_number',
            'handled_at' => Carbon::now()->subMinute()->toDateTimeString()
        ]);

        $response = $this->postJson('/api'.config('tsas.endpoint'), [
            'order' => [
                'uuid' => $uuid,
                'order_ref' => $known_order_ref,
                'status' => $known_status,
                'part_shipped' => false,
                'shipping_method' => 'Standard',
                'carrier' => null,
                'tracking_number' => 'test1234',
                'tracking_url' => null,
                'shipped_date' => null,
                'shipping_cost' => null,
                'items' => [
                    0 => [
                        'uuid' => Str::uuid(),
                        'item_ref' => 'test-item-ref',
                        'invoice_price' => 10.99
                    ]
                ]
            ]
        ], $this->headers());

        $response->assertStatus(200);
        $response->assertJsonFragment(['Update already processed']);
        $this->assertDatabaseCount('status_updates', 1);
        $this->assertDatabaseHas('status_updates', [
            'tracking_number' => 'test_tracking_number'
        ]);
        $this->assertDatabaseMissing('status_updates', [
            'tracking_number' => 'test1234'
        ]);
    }


    private function headers()
    {
        return [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer tshirtandsons',
            'Accept' => 'application/json'
        ];
    }
}
