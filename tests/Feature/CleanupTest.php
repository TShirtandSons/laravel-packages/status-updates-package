<?php

namespace TshirtAndSons\StatusUpdates\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use TshirtAndSons\StatusUpdates\Models\StatusUpdate;
use TshirtAndSons\StatusUpdates\Models\StatusUpdateItem;
use TshirtAndSons\StatusUpdates\Tests\StatusUpdatesTestCase;

class CleanupTest extends StatusUpdatesTestCase
{
    use RefreshDatabase;

    /** @test */
    public function cleanup_works_as_expected()
    {
        $days = 60;
        StatusUpdate::factory()
            ->count(500)
            ->create(
                [
                    'handled_at' => Carbon::today()->subDays(rand($days, 90))->toDateTimeString()
                ]
            )
            ->each(
                function ($update) use ($days) {
                    StatusUpdateItem::factory()
                        ->count(2)
                        ->create(
                            [
                                'status_update_id' => $update->id,
                            ]
                        );
                }
            );

        StatusUpdate::factory()
            ->count(200)
            ->create(
                [
                    'handled_at' => Carbon::today()->subDays(rand(1, $days - 1))->toDateTimeString()
                ]
            )
            ->each(
                function ($update) use ($days) {
                    StatusUpdateItem::factory()
                        ->count(2)
                        ->create(
                            [
                                'status_update_id' => $update->id,
                            ]
                        );
                }
            );

        $this->assertDatabaseCount('status_updates', 700);
        $this->assertDatabaseCount('status_update_items', 1400);

        $cleanup = StatusUpdate::cleanupHandledEntries(Carbon::now()->subDays($days));

        $this->assertDatabaseCount('status_updates', 200);
        $this->assertDatabaseCount('status_update_items', 400);

        $this->assertTrue($cleanup == 500);
    }

    /** @test */
    public function remove_tracking_number_works_as_expected()
    {
        $days = 60;
        StatusUpdate::factory()
            ->count(500)
            ->create(
                [
                    'status'          => 'shipped',
                    'tracking_number' => Str::random(10),
                    'handled_at'      => Carbon::today()->subDays(rand($days, 90))->toDateTimeString()
                ]
            )
            ->each(
                function ($update) use ($days) {
                    StatusUpdateItem::factory()
                        ->count(2)
                        ->create(
                            [
                                'status_update_id' => $update->id,
                            ]
                        );
                }
            );

        StatusUpdate::factory()
            ->count(200)
            ->create(
                [
                    'status'          => 'shipped',
                    'tracking_number' => Str::random(10),
                    'handled_at'      => Carbon::today()->subDays(rand(1, $days - 1))->toDateTimeString()
                ]
            )
            ->each(
                function ($update) use ($days) {
                    StatusUpdateItem::factory()
                        ->count(2)
                        ->create(
                            [
                                'status_update_id' => $update->id,
                            ]
                        );
                }
            );

        $this->assertDatabaseCount('status_updates', 700);
        $this->assertDatabaseCount('status_update_items', 1400);

        $removeTrackingNumbersCount = StatusUpdate::removeTrackingNumbersFromHandledShippedEntries(Carbon::now()->subDays($days));

        $this->assertTrue($removeTrackingNumbersCount === 500);

        $status_updates_without_tracking_numbers = StatusUpdate::where(
            'handled_at',
            '<=',
            Carbon::today()->subDays(
                $days
            )->toDateTimeString()
        )->get();

        foreach ($status_updates_without_tracking_numbers as $update) {
            $this->assertTrue($update->tracking_number == null);
        }
    }
}
