<?php

namespace TshirtAndSons\StatusUpdates\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Str;
use TshirtAndSons\StatusUpdates\Jobs\CleanUpHandledStatusUpdates;
use TshirtAndSons\StatusUpdates\Jobs\RemoveOldTrackingNumbersFromHandledShippedStatusUpdates;
use TshirtAndSons\StatusUpdates\Models\StatusUpdate;
use TshirtAndSons\StatusUpdates\Tests\StatusUpdatesTestCase;

class JobsTest extends StatusUpdatesTestCase
{
    use RefreshDatabase;

    /** @test */
    public function cleanup_job_is_dispatched_as_expected()
    {
        Bus::fake();

        StatusUpdate::factory()
            ->count(30)
            ->create(
                [
                    'tracking_number' => Str::random(10),
                    'status'          => 'shipped',
                ]
            );

        CleanUpHandledStatusUpdates::dispatch(Carbon::now()->subDays(30));

        Bus::assertDispatched(CleanUpHandledStatusUpdates::class);
    }

    /** @test */
    public function remove_tracking_numbers_job_is_dispatched_as_expected()
    {
        Bus::fake();

        StatusUpdate::factory()
            ->count(30)
            ->create(
                [
                    'tracking_number' => Str::random(10),
                    'status'          => 'shipped',
                ]
            );

        RemoveOldTrackingNumbersFromHandledShippedStatusUpdates::dispatch(Carbon::now()->subDays(30));

        Bus::assertDispatched(RemoveOldTrackingNumbersFromHandledShippedStatusUpdates::class);
    }
}
