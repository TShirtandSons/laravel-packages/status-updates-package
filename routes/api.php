<?php

use Illuminate\Support\Facades\Route;
use TshirtAndSons\StatusUpdates\Http\Controllers\StatusUpdateController;

Route::prefix('api')->post(config('tsas.endpoint'), [StatusUpdateController::class, 'store']);
