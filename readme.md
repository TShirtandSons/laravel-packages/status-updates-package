# Status Updates

## Concepts

This package loads the route ```Route::post('/api/receive-tss-status-update', 'TshirtAndSons\StatusUpdateController@store')```. 

You can override the name of the route by adding a .env key `TSAS_STATUS_UPDATE_ENDPOINT`, e.g. `TSAS_STATUS_UPDATE_ENDPOINT=/new-route-name`. **DO NOT PREFIX** `api/` to this as this has already been done for you.

```tshirtandsons_status_updates``` and ```tshirtandsons_status_update_items``` migrations are included enabling status updates to be stored for reconciliation purposes.

## Installation & Setup
First make sure you have a personal access token setup on GitLab with at least the read_api scope - [Setup Guide](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)

Add the below to the composer.json for your repo

```yaml
"repositories": [
    {
        "type": "composer",
        "url": "https://gitlab.com/api/v4/group/10575089/-/packages/composer/packages.json"
    }
],
```

Then run the following command to generate the auth.json replacing the <ACCESS_TOKE> with on generate from your [GitLab account](https://gitlab.com/-/profile/personal_access_tokens)

```bash
composer config http-basic.gitlab.com ___token___ <ACCESS_TOKEN>
```

Now just run the composer require command from your terminal:

```bash
composer require t-shirt-and-sons/status-updates
```

Publish the package's Service Provider:
```bash 
php artisan vendor:publish --provider="TshirtAndSons\StatusUpdates\StatusUpdateServiceProvider"
```

In your application's `.env` file, create a `TSAS_BEARER_TOKEN` key and set its value to an appropriate token. Remember to share this with your technical contact at T Shirt & Sons, so it can be added to their status updates application. 

Run migrations:
```bash
php aritsan migrate
```

## Usage Examples

### When we receive a status update, I want to update our system using the information provided
When an update is received the `StatusUpdateWasReceived` event is fired which you can listen to and update your records as needed, to do this you will want to generate a listener with the below command.

```bash
php artisan make:listener StatusUpdateListener --event=\\TshirtAndSons\\StatusUpdates\\Events\\StatusUpdateWasReceived
```

After this has been generated, you can now register this listener against the event in your `EventServiceProvider` as seen below

```php
protected $listen = [
    \TshirtAndSons\StatusUpdates\Events\StatusUpdateWasReceived::class => [
        \App\Listeners\StatusUpdateListener::class,
    ],
];
```

Below is an example of what you could put in the handle function of your listener

```php
public function handle(TshirtAndSons\StatusUpdates\Events\StatusUpdateWasReceived $event)
{
    /*
     * Here we expect you will find your order, and update fields accordingly.
     * You are free to trigger other events, dispatch jobs, or perform any other operations you may need to.
     *
     * See the package documentation for an up-to-date list of possible statuses, carriers, shipping methods.
     * Any other statuses we may have agreed to send you can be considered included in the list.
     */
     
    $order = Order::where('order_ref', $event->statusUpdate->order_ref);
    $order->status = $event->statusUpdate->status;

    if ($event->statusUpdate->status == 'shipped') {
        $order->tracking_number = $event->statusUpdate->tracking_number;
        $order->carrier_used = $event->statusUpdate->carrier;
        $order->shipping_cost = $event->statusUpdate->shipping_cost;
    }

    // ===========================================================================
    // This records when the status update was handled and must be kept unchanged.
    // ===========================================================================
    $event->statusUpdate->handled_at = \Illuminate\Support\Carbon::now()->toDateTimeString();
    $event->statusUpdate->save();
}
```

### What is the most recent status received for an order?

This will return a StatusUpdate object (or null if non exist) for your order ref as supplied.
```php
$statusUpdate = TshirtAndSons\StatusUpdates\Models\StatusUpdate::getLatestForOrderRef($orderRef);
```

### I want to see all status updates received for an order in the order they were received

This will return a collection of Status Update objects.
```php
$statusUpdates = TshirtAndSons\StatusUpdates\Models\StatusUpdate::listForOrderRef($orderRef);
```

### I want to relate status updates received to our Order model
Include the `HasStatusUpdates` trait into your order model, you can then call the `statusUpdates` method on the model.

```php
class Order extends Illuminate\Database\Eloquent\Model
{
    use TshirtAndSons\StatusUpdates\Traits\HasStatusUpdates;
}

$orderStatusUpdates = Order::find(1)->statusUpdates;
```

### I want to clean up status updates that have been handled from a given date

This function takes in the date you wish to clean up from and returns the number of entries that where processed.

```php
TshirtAndSons\StatusUpdates\Models\StatusUpdate::cleanupHandledEntries(
    \Illuminate\Support\Carbon::now()->subDays(2)
);
```

You may wish to dispatch a Job to handle this, depending on the number of status updates you have, and this package includes such a job
```php
\TshirtAndSons\StatusUpdates\Jobs\CleanUpHandledStatusUpdates::dispatch(
    \Illuminate\Support\Carbon::now()->subDays(6)
);
```

### I want to remove tracking numbers for shipped status updates handled more than X days ago

Tracking numbers can be considered "personally identifiable information" under GDPR, and as such you may wish to remove them for shipped updates older than a given date.

This function takes in the date you wish to remove tracking numbers from and returns the number of entries that where processed.

```php
\TshirtAndSons\StatusUpdates\Models\StatusUpdate::removeTrackingNumbersFromHandledShippedEntries(
    \Illuminate\Support\Carbon::now()->subDays(6)
);
```

You may wish to dispatch a Job to handle this, depending on the number of status updates you have, and this package includes such a job
```php
\TshirtAndSons\StatusUpdates\Jobs\RemoveOldTrackingNumbersFromHandledShippedStatusUpdates::dispatch(
    \Illuminate\Support\Carbon::now()->subDays(6)
);
```

## StatusUpdate Available Properties

Below is a list of the properties that can be called on the StatusUpdate model

* id
* order_id
* order_ref
* part_shipped
* status
* shipping_method
* carrier
* tracking_number
* tracking_url
* date_shipped
* shipping_cost
* handled_at
* created_at
* updated_at

### Possible Options

#### Statuses

* picked
* printed
* shipped

#### Shipping Methods

* Standard
* Standard_Tracked
* Standard_First_Class
* Express

#### Carriers

* Mail International
* Royal Mail
* DPD UK
* DPD NL
* Asendia

## StatusUpdateItem Available Properties

* id
* status_update_id
* item_ref
* invoice_price
* created_at
* updated_at
